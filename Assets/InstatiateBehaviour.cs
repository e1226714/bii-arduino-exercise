﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
public class InstatiateBehaviour : MonoBehaviour {



    // various gameobject prefabs to instantiate
    public GameObject brickPrefab = null;
    public GameObject cylinderPrefab = null;
    public GameObject holzPrefab = null;
    public GameObject spherePrefab = null;

    public int item = 0;
    public GameObject test;
    private Vector3 testPos;

    public int baudRate = 0;
    public string port = null;
    private Vector3 rotation = new Vector3(0f, 90f, 0f);
    
        // Connection to Arduino
    SerialPort sp = new SerialPort("COM3",9600);

	// Use this for initialization


	void Start ()
    {
        sp.Open();
        sp.ReadTimeout = 1;
        	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("1"))
        {
            item = 1;
        }

        if (Input.GetButtonDown("2"))
        {
            item = 2;
        }
        if (Input.GetButtonDown("3"))
        {
            item = 3;
        }
        if (Input.GetButtonDown("4"))
        {
            item = 4;
        }

        if (sp.IsOpen)
        {
            try
            {
               
                InstantiateRFIDObject(sp.ReadByte());
             
            }
            catch (System.Exception)
            {
                Debug.Log("running");
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {

            var mousePos = Input.mousePosition;
            mousePos.z = 5.0f;
            testPos = Camera.main.ScreenToWorldPoint(mousePos);

            if (item == 1)
            {
                Instantiate(brickPrefab, testPos, Quaternion.identity);
            }

            if (item == 2)
            {
                Instantiate(cylinderPrefab, testPos, Quaternion.identity);
            }

            if (item == 3)
            {
                Instantiate(holzPrefab, testPos, Quaternion.Euler(rotation));
            }

            if (item == 4)
            {
                Instantiate(spherePrefab, testPos, Quaternion.identity);
            }


        }
    }


    void InstantiateRFIDObject(int tagId)
    {
        // Debug.Log("in der Methode");
        if (tagId == 1) // Ziegelstein 352309C  55718044
        {
            item = 1;
            // Debug.Log("test");
            // Instantiate(brickPrefab, new Vector3(0,1,0), Quaternion.identity);
        }

        if (tagId == 2) // Ziegelsteinhalbe 34A4288  55198344

        {
            item = 2;
            Debug.Log("half brick");
        }

        if (tagId == 3) // Fenster 8C4F6B  9195371

        {
            item = 3;
            Debug.Log("window");
        }

        if (tagId == 4) // Tür 19AFAA3  26933923

        {
            item = 4;
            Debug.Log("door");
        }

    }

    void InstantiateWithoutArduino(int tagId)
    {
       
        if (tagId == 1) // Ziegelstein 352309C  55718044
        {
            item = 1;
           
        }

        if (tagId == 2) 

        {
            item = 2;
           
        }

        if (tagId == 3) // Fenster 8C4F6B  9195371

        {
            item = 3;
           
        }

        if (tagId == 4) // Tür 19AFAA3  26933923

        {
            item = 4;
          
        }

    }
}
