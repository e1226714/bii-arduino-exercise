#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX --> DOUT, TX --> DIN
void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  delay(1000);
  mySerial.println("test");
  Serial.println("-- " + mySerial.available());
  if (mySerial.available()) {
    Serial.println("available");
  }
  else {
    Serial.println("not available");
  }
}
