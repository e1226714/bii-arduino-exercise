/*
  SendATweet

  Demonstrates sending a tweet via a Twitter account using the Temboo Arduino Yun SDK.

  This example code is in the public domain.
*/

#include <AltSoftSerial.h>
#include <Bridge.h>
#include <Temboo.h>
#include "TembooAccount.h" // contains Temboo account information

/*** SUBSTITUTE YOUR VALUES BELOW: ***/

// Note that for additional security and reusability, you could
// use #define statements to specify these values in a .h file.
const String TWITTER_ACCESS_TOKEN = "187850728-lrIHGzr3W72dpDgagF04n6I20lkBzOF4Gjb5U1KN";
const String TWITTER_ACCESS_TOKEN_SECRET = "OG3RMm64kU0N88yijFUwo6PUJP0XLD6mq5ztkXkMkXZMo";
const String TWITTER_API_KEY = "GN0OXi209ysCR5KBfwhmjepMn";
const String TWITTER_API_SECRET = "aKlEKuAmLqv8kdFiQa0L3PDR8TkdG0T3t6wrR4jf389Qx2WU5y";

AltSoftSerial altSerial;

int incomingByte ;

bool doorOpen = true;
bool changed = false;

String tweetText = "";

void setup() {
  Serial.begin(9600);
  while (!Serial) ; // wait for Arduino Serial Monitor to open
  Serial.println("AltSoftSerial Test Begin");
  altSerial.begin(9600);

  Bridge.begin();
}

void loop()
{
  //Serial.println(altSerial.available() > 0);

  
  while (altSerial.available() > 0) {

    incomingByte = altSerial.read();
    

    

    if (incomingByte == 48) {
      if (doorOpen) {
        changed = true;
      }
      doorOpen = false;
      tweetText = "Door has been closed at " + String(millis());

    } else {
      if (!doorOpen) {
        changed = true;
      }
      doorOpen = true;
      tweetText = "Door has been opened at " + String(millis());
    }


    if (changed) {
      changed = false;

      Serial.println(tweetText);

      // define the text of the tweet we want to send
      //String tweetText("My Arduino Yun has been running for " + String(millis()) + " milliseconds.");


      TembooChoreo StatusesUpdateChoreo;

      // invoke the Temboo client
      // NOTE that the client must be reinvoked, and repopulated with
      // appropriate arguments, each time its run() method is called.
      StatusesUpdateChoreo.begin();

      // set Temboo account credentials
      StatusesUpdateChoreo.setAccountName(TEMBOO_ACCOUNT);
      StatusesUpdateChoreo.setAppKeyName(TEMBOO_APP_KEY_NAME);
      StatusesUpdateChoreo.setAppKey(TEMBOO_APP_KEY);

      // identify the Temboo Library choreo to run (Twitter > Tweets > StatusesUpdate)
      StatusesUpdateChoreo.setChoreo("/Library/Twitter/Tweets/StatusesUpdate");

      // set the required choreo inputs
      // see https://www.temboo.com/library/Library/Twitter/Tweets/StatusesUpdate/
      // for complete details about the inputs for this Choreo

      // add the Twitter account information
      StatusesUpdateChoreo.addInput("AccessToken", TWITTER_ACCESS_TOKEN);
      StatusesUpdateChoreo.addInput("AccessTokenSecret", TWITTER_ACCESS_TOKEN_SECRET);
      StatusesUpdateChoreo.addInput("ConsumerKey", TWITTER_API_KEY);
      StatusesUpdateChoreo.addInput("ConsumerSecret", TWITTER_API_SECRET);

      // and the tweet we want to send
      StatusesUpdateChoreo.addInput("StatusUpdate", tweetText);

      // tell the Process to run and wait for the results. The
      // return code (returnCode) will tell us whether the Temboo client
      // was able to send our request to the Temboo servers
      unsigned int returnCode = StatusesUpdateChoreo.run();

      // a return code of zero (0) means everything worked
      if (returnCode == 0) {
        Serial.println("Success! Tweet sent!");
      } else {
        // a non-zero return code means there was an error
        // read and print the error message
        while (StatusesUpdateChoreo.available()) {
          char c = StatusesUpdateChoreo.read();
          Serial.print(c);
        }
      }
      StatusesUpdateChoreo.close();


      //changed = false;
      // do nothing for the next 5 seconds
      //Serial.println("Waiting...");
      delay(1000);

    }

    
  }

  delay(1000);
}
