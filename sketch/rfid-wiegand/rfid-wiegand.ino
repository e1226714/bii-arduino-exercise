/* Crazy People
 * By Mike Cook April 2009
 * RFID reader outputing 26 bit Wiegand code to pins:-
 * Reader (Head) Pins 2 & 3
 * Interrupt service routine gathers Wiegand pulses (zero or one) until 26 have been recieved
 * Then a sting is sent to processing
 * Modified FG
 */
 
 
 /* Dieser Code ist für Wiegand Protokoll RIFD Reader geschrieben
    D.h. das Signal kommt nicht über die UART Schnittstelle vom Reader zum
    Arduino, sondern über Interrupt-Pins. Dies hat den Vorteil, dass die UART
    Schnittstelle frei bleibt */

volatile long reader = 0; 
volatile int readerCount = 0;
volatile int readerCountbin[26];

void readerOne(void) {
  readerCountbin[readerCount] = 1; 
  readerCount++;
  reader = reader << 1;
  reader |= 1;
}

void readerZero(void) {
  readerCountbin[readerCount] = 0; 
  readerCount++;
  reader = reader << 1;  
}


void setup()
{
  Serial.begin(9600);
  // Attach pin change interrupt service routines from the Wiegand RFID readers
  attachInterrupt(0, readerZero, FALLING);//DATA0 to pin 2
  attachInterrupt(1, readerOne, FALLING); //DATA1 to pin 3
  delay(10);
  // the interrupt in the Atmel processor mises out the first negitave pulse as the inputs are already high,
  // so this gives a pulse to each reader input line to get the interrupts working properly.
  // Then clear out the reader variables.
  // The readers are open collector sitting normally at a one so this is OK
  for(int i = 2; i<4; i++){
    pinMode(i, OUTPUT);
    digitalWrite(i, HIGH); // enable internal pull up causing a one
    digitalWrite(i, LOW); // disable internal pull up causing zero and thus an interrupt
    pinMode(i, INPUT);
    digitalWrite(i, HIGH); // enable internal pull up
  }
  delay(10);
  // put the reader input variables to zero
  reader = 0;
  readerCount = 0;
  digitalWrite(13, HIGH);  // show Arduino has finished initilisation
}

void loop() {
  if(readerCount >= 26){
    //String str = strtoul(reader);
    //Serial.println(reader);
    if(reader == 55718044){
      Serial.write(1);
    } else if(reader == 55198344){
      Serial.write(2);
    } else if(reader == 9195371){
      Serial.write(3);
    } else if(reader == 26933923){
      Serial.write(4);  
    }
    Serial.flush();
    delay(100);
    //Serial.print("Reader HEX ");Serial.println(reader,HEX);
    //Serial.println("Reader DEC");
    //Serial.println(reader & 0xfffffff); // bitweise AND mit 11111.......11 
    /*Serial.println("Binary begin");                         // 0xfffffff (HEX) == 268435455 (DEC)             
    for (int k=0; k<26; k++) {
      Serial.print(readerCountbin[k]);
      readerCountbin[k]=9;
    }
    Serial.println("");
    Serial.println("Binary end");*/
    reader = 0;
    readerCount = 0;
  }    
}
